package main

import ( "net/http"
		 "time" 
		 "log"
		 "fmt")

func main() {

	StartServer()
	
	// Essa linha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func cebolas(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte("Teste"))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.26:8082",
			IdleTimeout: duration, 
	}

	http.HandleFunc("/cebola", cebolas)
	http.HandleFunc("/", showUrl)

	log.Print(server.ListenAndServe())
}

func showUrl (w http.ResponseWriter, r *http.Request){
	fmt.Fprint(w, r.Method + " " + r.URL.Path + "?",r.URL.RawQuery)

}